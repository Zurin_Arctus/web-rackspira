<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Front_model extends CI_Model {



	public $variable;



	public function __construct()
	{
		parent::__construct();


	}

public function count_where($table, $field, $isi){
	$this->db->where($field,$isi);
	$this->db->from($table);

	return $this->db->count_all_results();
}

public function count_where_2($table, $field1, $isi1, $field2, $isi2){
	$this->db->where($field1,$isi1);
	$this->db->where($field2,$isi2);
	$this->db->from($table);

	return $this->db->count_all_results();
}

public function count_like($table, $field, $isi){
	$this->db->like($field,$isi);
	$this->db->from($table);

	return $this->db->count_all_results();
}

	//--------------------METHOD CARI --------------------
	public function cari($table, $field, $isi, $limit, $start){

		$this->db->from($table);
		$this->db->like($field,$isi);
		$this->db->limit($limit, $start);
		$query=$this->db->get();

		return $query->result();
	}
	//--------------------METHOD GET WHERE --------------------

	public function get_where($table,$field,$isi){
		$this->db->from($table);
		$this->db->where($field,$isi);
		$query=$this->db->get();

		return $query->result();
	}

	public function get_where_limit($table,$field,$isi,$limit, $start){
		$this->db->from($table);
		$this->db->where($field,$isi);
		$this->db->limit($limit, $start);
		$query=$this->db->get();

		return $query->result();
	}

	// ----------------METHOD GET-------------------------

	public function get_data($table){
		$this->db->from($table);
		$query=$this->db->get();

		return $query->result();
	}

	public function get_data_artikel($limit, $start){
		$query=$this->db->query(
			"SELECT artikel.*, kat.* FROM tb_artikel artikel
			INNER JOIN tb_kategori kat ON artikel.id_kategori = kat.id_kategori
			ORDER BY artikel.id_artikel DESC LIMIT ".$start.",".$limit
		);

		return $query->result();
	}

	public function get_data_artikel_per_kategori($kat,$limit, $start){
		$query=$this->db->query(
			"SELECT artikel.*, kat.* FROM tb_artikel artikel
			INNER JOIN tb_kategori kat ON artikel.id_kategori = kat.id_kategori
			WHERE artikel.id_kategori=".$kat."
			ORDER BY artikel.id_artikel DESC LIMIT ".$start.",".$limit
		);

		return $query->result();
	}

	public function get_data_arsip($bulan, $tahun, $limit, $start){
		$query=$this->db->query(
			"SELECT artikel.*, kat.* FROM tb_artikel artikel
			INNER JOIN tb_kategori kat ON artikel.id_kategori = kat.id_kategori
			WHERE MONTH(artikel.tgl)=".$bulan." AND YEAR(artikel.tgl)=".$tahun."
			ORDER BY artikel.id_artikel DESC LIMIT ".$start.",".$limit
		);

		return $query->result();
	}

	public function get_artikel_single($url){
		$query=$this->db->query(
			"SELECT artikel.*, kat.* FROM tb_artikel artikel
			INNER JOIN tb_kategori kat ON artikel.id_kategori = kat.id_kategori
			WHERE artikel.url='".$url."'"
		);

		return $query->result();
	}

	public function get_data_limit($table, $limit, $start){
		$this->db->from($table);
		$this->db->limit($limit, $start);
		$query=$this->db->get();

		return $query->result();
	}

	public function get_data_order($table, $key, $order){
		$this->db->from($table);
		$this->db->order_by($key, $order);
		$query=$this->db->get();

		return $query->result();
	}

	public function get_where_order($table, $field, $isi, $key, $order){
		$this->db->from($table);
		$this->db->where($field,$isi);
		$this->db->order_by($key, $order);
		$query=$this->db->get();

		return $query->result();
	}






	// ----------------METHOD INSERT-------------------------
	public function insert($table, $data)
	{
		$this->db->trans_start();
		$this->db->insert($table, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			return false;
		} else {
			return true;
		}
	}



	// ----------------METHOD DEL-------------------------
	public function delete_order($id)
	{
  		$this->db->where('id', $id);
  		$this->db->delete('tb_order');
	}

	// ----------------METHOD UPDATE-------------------------
	public function update_member($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('tb_member', $data);
	}

	public function update_konfirmasi($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('tb_order', $data);
	}
}



/* End of file db_rental.php */

/* Location: ./application/models/db_rental.php */
