    <div class="row">
      <div class="col-md-12">
				            <div class="card">
                        <div class="card-header" data-background-color="red">
                          <h4 class="card-title">Mantap Jiwa - Rack Spira's Admin Page</h4>
                        </div>
				                <div class="card-content">
				                    <div class="places-buttons">
				                        <div class="row">
				                            <div class="col-md-8 col-md-offset-2 text-center">
				                                <h4 class="card-title">
				                                    Selamat Datang di Halaman Admin
				                                    <p class="category" align="justify">
				                                      Anda telah berhasil login sebagai Admin sehingga bisa mengakses halaman ini. Beberapa keterangan di bawah ini menunjukkan statistik konten yang ada pada halaman depan website Rack Spira.
				                                    </p>
				                                </h4>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="orange">
            <i class="material-icons">book</i>
          </div>
          <div class="card-content">
            <p class="category">Artikel</p>
            <h3 class="card-title"><?php echo $artikel; ?></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">assignment_turned_in</i> Artikel yang telah dibuat
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="rose">
            <i class="material-icons">card_membership</i>
          </div>
          <div class="card-content">
            <p class="category">Kategori Artikel</p>
            <h3 class="card-title"><?php echo $kategori; ?></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">assignment</i> Kategori artikel yang tersedia
            </div>
          </div>
        </div>
      </div>


      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="green">
            <i class="material-icons">filter_vintage</i>
          </div>
          <div class="card-content">
            <p class="category">Karya</p>
            <h3 class="card-title"><?php echo $karya; ?></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">brightness_low</i> Karya yang telah dihasilkan
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header" data-background-color="blue">
            <i class="material-icons">opacity</i>
          </div>
          <div class="card-content">
            <p class="category">Kategori Karya</p>
            <h3 class="card-title"><?php echo $kategori_karya; ?></h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">graphic_eq</i> Kategori hasil karya yang tersedia
            </div>
          </div>
        </div>
      </div>
    </div>
