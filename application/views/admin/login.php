<div class="row">
    <div class="col-md-10 col-md-offset-1">

        <div class="card card-signup">
            <h2 class="card-title text-center">Login</h2>
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <div class="card-content">
                        <div class="info info-horizontal">
                            <div class="icon icon-rose">
                                <i class="material-icons">build</i>
                            </div>
                            <div class="description">
                                <h4 class="info-title">Manipulasi</h4>
                                <p class="description" align="justify">
                                    Dengan melakukan login ke halaman Admin Anda dapat melakukan manipulasi terhadap konten yang ditampilkan pada halaman depan website.
                                </p>
                            </div>
                        </div>

                        <div class="info info-horizontal">
                            <div class="icon icon-info">
                                <i class="material-icons">group</i>
                            </div>
                            <div class="description">
                                <h4 class="info-title">Konfigurasi Akun Admin</h4>
                                <p class="description" align="justify">
                                    Panel Admin juga dapat digunakan untuk mengkonfigurasi akun admin bersangkutan yang digunakan untuk login.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="social text-center">
                        <a href="https://facebook.com/RackSpiraFans" target="_blank">
                        <button class="btn btn-just-icon btn-round btn-facebook">
                            <i class="fa fa-facebook"> </i>
                        </button>
                        </a>
                        <a href="https://instagram.com/RackSpira" target="_blank">
                        <button class="btn btn-just-icon btn-round btn-dribbble">
                            <i class="fa fa-instagram"></i>
                        </button>
                        </a>
                    </div>

                    <form id="LoginValidation" class="form" method="post" action="<?php echo base_url(); ?>lancelot/login_proses">
                        <div class="card-content">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">account_box</i>
                                </span>
                                <div class="form-group label-floating">
                                  <label class="control-label">Username</label>
                                  <input type="text" name="username" class="form-control" required="true">
                                </div>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <div class="form-group label-floating">
                                  <label class="control-label">Password</label>
                                  <input type="password" name="password" class="form-control" required="true"/>
                                </div>
                            </div>

                        </div>
                        <div class="footer text-center">
                            <button type="submit" name="login" class="btn btn-primary btn-round">Login</button>
                            <button type="reset" name="reset" class="btn btn-danger btn-round">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
