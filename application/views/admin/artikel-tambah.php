<div class="row">
  <div class="col-md-12">
							<div class="card">
								<form method="post" action="<?php echo base_url(); ?>arthur/artikel_proses" class="form-horizontal">
									<div class="card-header card-header-text" data-background-color="rose">
										<h4 class="card-title"><i class="material-icons">library_add</i> Tambah Artikel</h4>
									</div>
									<div class="card-content">
										<div class="row">
											<label class="col-sm-2 label-on-left">Judul Artikel</label>
				                <div class="col-sm-10">
  												<div class="form-group label-floating is-empty">
  													<label class="control-label">Masukkan judul artikel</label>
  													<input type="text" name="judul" class="form-control" required="true">
                          </div>
				                </div>
										</div>

                    <div class="row">
											<label class="col-sm-2 label-on-left">Kategori Artikel</label>
				                <div class="col-sm-4">
  												<div class="form-group label-floating is-empty">
  													<select class="form-control" name="kategori">
                              <?php foreach ($kategori as $key => $value) { ?>
                              <option value="<?php echo $value->id_kategori; ?>"><?php echo $value->judul_kategori; ?></option>
                              <?php } ?>
  													</select>
                          </div>
				                </div>
										</div>

                    <div class="row">
                      <div class="col-sm-10 col-sm-offset-1">
                        <button type="submit" class="btn btn-fill btn-rose">Submit<div class="ripple-container"></div></button>
                      </div>
                    </div>

									</div>
								</form>
							</div>
						</div>
</div>
