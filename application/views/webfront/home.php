<!--Slider-->
<div class="slider">

  <!--Main slider-->
  <div class=" mainSlider flexslider">




    <ul class="slides">

      <li><img src="<?php echo base_url(); ?>assets/themes/webfront/images/sliderImages/slide1.jpg" class="slide" alt=""/>
          <div class="caption">
            <h1>Selamat Datang di Web Kami</h1>
            <p>Website ini adalah situs resmi dari komunitas Rack Spira. Kami merupakan komunitas yang mewadahi kegiatan sharing atau
            belajar bersama dalam bidang teknologi dan informasi.
            </p>
            <div class="ctl">
              <a class="folioSh btn scroll " href="<?php base_url(); ?>home/artikel">Lihat Artikel</a>
              <a class="contact btn scroll " href="<?php base_url(); ?>home/about">Tentang Kami</a>
            </div>
          </div>
      </li>
    </ul>



  </div>
  <!--End main slider-->
</div>
<!--End  slider-->






<!--About section-->
  <section id="about">




        <!--Expertise -->
      <div class="expertise tCenter margTop  ">



          <!--Big title-->
          <div class="bigTitle">
          <h1>Komunitas Kami Membahas</h1>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/star.png" alt="">
          </div>
          <!--End big title-->


            <!--Container-->
            <div class="container clearfix ">

                <!--Experstise content-->
                <div class="expertsiseContent clearfix">

                  <!--Expertise-->
                  <div class="one-third columns exp ">
                    <div class="expIco">
                    <i class="fa fa-android" style="color:#2ecc71;"></i>
                    </div>

                    <div class="expDet">
                      <h3>Android Dev</h3>
                      <p>Pengembangan aplikasi atau game mobile berbasis Android baik menggunakan bahasa native maupun hybrid.  </p>
                    </div>

                  </div>
                  <!--End expertise-->


                <!--Expertise-->
                <div class="one-third columns exp ">
                  <div class="expIco">
                    <i class="fa fa-desktop" style="color:#3498db;"></i>
                  </div>

                  <div class="expDet">
                    <h3>Desktop Dev</h3>
                    <p>Pengembangan aplikasi yang berjalan pada platform desktop yang interaktif dan mudah digunakan. </p>
                  </div>

                </div>
                <!--End expertise-->



                  <!--Expertise-->
                  <div class="one-third columns exp ">
                    <div class="expIco">
                      <i class="fa fa-photo" style="color:#e67e22;"></i>
                    </div>

                    <div class="expDet">
                      <h3>Desain Grafis &amp; Animasi</h3>
                      <p>Meliputi desain vektor, vexel, 3D, animasi, dsb yang meliputi karya seni berbentuk digital .  </p>
                    </div>

                  </div>
                  <!--End expertise-->

                </div>
                <!--End experstise content-->


                <!--Experstise content-->
                <div class="expertsiseContent clearfix margBottom">

                  <!--Expertise-->
                  <div class="one-third columns exp ">
                    <div class="expIco">
                    <i class="fa fa-css3" style="color:#2980b9;"></i>
                    </div>

                    <div class="expDet">
                      <h3>Desain Web</h3>
                      <p>Pembuatan desain tampilan front-end website sehingga dapat meningkatkan kepuasan UI/UX.  </p>
                    </div>

                  </div>
                  <!--End expertise-->


                <!--Expertise-->
                <div class="one-third columns exp ">
                  <div class="expIco">
                    <i class="fa fa-fire" style="color:#e74c3c;"></i>
                  </div>

                  <div class="expDet">
                    <h3>Web Dev</h3>
                    <p>Pengembangan aplikasi berbasis web secara sistem untuk membuat web yang dinamis dan interaktif untuk digunakan.  </p>
                  </div>

                </div>
                <!--End expertise-->



                  <!--Expertise-->
                  <div class="one-third columns exp ">
                    <div class="expIco">
                      <i class="fa fa-exchange" style="color:#34495e;"></i>
                    </div>

                    <div class="expDet">
                      <h3>Networking</h3>
                      <p>Membahas materi terkait jaringan baik pembangunan arsitektur jaringan maupun dari segi keamanan.  </p>
                    </div>

                  </div>
                  <!--End expertise-->

                </div>
                <!--End experstise content-->

            </div>
            <!--End container-->


      </div>
      <!--End expertise -->


      <!--Last work holder-->
    <div class="lastWorkHolder tCenter margTop">

          <!--Title grey holder -->
        <div class="tgreyHolder bgGrey ofsInTop">


          <!--Big title-->
          <div class="bigTitle tCenter">
          <h1>Karya Kami</h1>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/star.png" alt="">
          </div>
          <!--End big title-->


      </div>
      <!--End title grey holder-->


        <!--Last work -->
        <ul class="owl-slider latestWork owl-carousel clearfix">

          <!--Latest -->
          <li class="latest">

            <!--Latest desc-->
            <div class="latestDesc">
              <div class="latestDescInner">
              <h3>Mobile App<span> - graphic design</span></h3>
                <a  class="btn latestBtn" href="project.html">Launch Project</a>
              </div>
            </div>
            <!--End latest desc-->

            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt2.jpg" alt="">
          </li>
          <!--End latest-->


          <!--Latest-->
          <li class="latest">

            <!--Latest desc-->
            <div class="latestDesc">
              <div class="latestDescInner">
              <h3>Cv Mockup<span> - motion design</span></h3>
                <a  class="btn latestBtn" href="project.html">Launch Project</a>
              </div>
            </div>
            <!--End latest desc-->


            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt1.jpg" alt="">
          </li>
          <!--End latest-->


          <!--Latest -->
          <li class="latest">

            <!--Latest desc-->
            <div class="latestDesc">
              <div class="latestDescInner">
              <h3>Cd Room<span> - web design</span></h3>
                <a  class="btn latestBtn" href="project.html">Launch Project</a>
              </div>
            </div>
            <!--End latest desc-->

            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt3.jpg" alt="">

          </li>
          <!--End latest-->

            <!--Latest -->
            <li class="latest">

              <!--Latest desc-->
              <div class="latestDesc">
                <div class="latestDescInner">
                <h3>Cd Room<span> - web design</span></h3>
                  <a  class="btn latestBtn" href="project.html">Launch Project</a>
                </div>
              </div>
              <!--End latest desc-->

              <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt4.jpg" alt="">

            </li>
            <!--End latest-->



                    <!--Latest -->
                    <li class="latest">

                      <!--Latest desc-->
                      <div class="latestDesc">
                        <div class="latestDescInner">
                        <h3>Mobile App<span> - graphic design</span></h3>
                          <a  class="btn latestBtn" href="project.html">Launch Project</a>
                        </div>
                      </div>
                      <!--End latest desc-->

                      <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt4.jpg" alt="">
                    </li>
                    <!--End latest-->


                    <!--Latest-->
                    <li class="latest">

                      <!--Latest desc-->
                      <div class="latestDesc">
                        <div class="latestDescInner">
                        <h3>Cv Mockup<span> - motion design</span></h3>
                          <a  class="btn latestBtn" href="project.html">Launch Project</a>
                        </div>
                      </div>
                      <!--End latest desc-->


                      <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt6.jpg" alt="">
                    </li>
                    <!--End latest-->


                    <!--Latest -->
                    <li class="latest">

                      <!--Latest desc-->
                      <div class="latestDesc">
                        <div class="latestDescInner">
                        <h3>Cd Room<span> - web design</span></h3>
                          <a  class="btn latestBtn" href="project.html">Launch Project</a>
                        </div>
                      </div>
                      <!--End latest desc-->

                      <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt7.jpg" alt="">

                    </li>
                    <!--End latest-->

                      <!--Latest -->
                      <li class="latest">

                        <!--Latest desc-->
                        <div class="latestDesc">
                          <div class="latestDescInner">
                          <h3>Cd Room<span> - web design</span></h3>
                            <a  class="btn latestBtn" href="project.html">Launch Project</a>
                          </div>
                        </div>
                        <!--End latest desc-->

                        <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/lt8.jpg" alt="">

                      </li>
                      <!--End latest-->



        </ul>
        <!--End last work-->

    </div>
      <!--End last work holder-->





        <!--Team-->
              <div class="team margHBottom tCenter ofsInTop">

                  <!--Big title-->
                  <div class="bigTitle">
                  <h1>Anggota Inti</h1>
                  <img src="<?php echo base_url(); ?>assets/themes/webfront/images/star.png" alt="">
                  </div>
                  <!--End big title-->

              <!--Container-->
              <div class="container clearfix ">


                <div class="smallIntro margBottom">
                  <p>Anggota inti meliputi sejumlah orang yang aktif dalam komunitas kami
                  </p>
                </div>


                <div class="teamContent clearfix">
                        <!--Team-->
                        <div class="four columns teamLead ">
                          <div class="tImg"><img class="super" alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/yudis.jpg"></div>

                          <!--Team desc-->
                          <div class="teamDesc">
                            <h3>Yudistiro Septian Dwi Saputro<span><i class="fa fa-star"></i> Ketua | Android Developer</span></h3>
                            <ul class="tSocials">
                              <li><a href="https://www.facebook.com/yudistiro15" target="_blank"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="https://www.instagram.com/blankon15/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                          </div>
                          <!--End team desc-->

                        </div>
                        <!--End team-->


                        <!--Team-->
                        <div class="four columns teamSingle ">
                          <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/yudha.jpg"></div>

                            <!--Team desc-->
                            <div class="teamDesc">
                              <h3>Yudha Pratama Putra<span>Android Developer</span></h3>
                              <ul class="tSocials">
                              <li><a href="https://www.facebook.com/Kudkud32" target="_blank"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="https://www.instagram.com/joedha_alzelvin/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                              </ul>
                            </div>
                            <!--End team desc-->

                        </div>
                        <!--End team-->


                        <!--Team-->
                        <div class="four columns teamSingle ">
                          <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/gambreng.jpg"></div>


                          <!--Team desc-->
                          <div class="teamDesc">
                            <h3>Bahtiyar Istiyarno<span>IT Security</span></h3>
                            <ul class="tSocials">
                            <li><a href="https://www.facebook.com/akun.telahdihapus" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/pemburuputih/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                          </div>
                          <!--End team desc-->

                        </div>
                        <!--End team-->


                        <!--Team-->
                        <div class="four columns teamSingle ">
                          <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/koko.jpg"></div>

                          <!--Team desc-->
                          <div class="teamDesc">
                            <h3>Muhammad Kodrat Santoso<span>Web Developer</span></h3>
                            <ul class="tSocials">
                              <li><a href="https://www.facebook.com/kodratmeden" target="_blank"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                          </div>
                          <!--End team desc-->

                        </div>
                        <!--End team-->


                      </div>



                    <div class="teamContent clearfix ">
                            <!--Team-->
                            <div class="four columns teamSingle ">
                              <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/lutfi.jpg"></div>

                              <!--Team desc-->
                              <div class="teamDesc">
                                <h3>Kristiawan Adi Lutvianto<span>Full Stack Programmer</span></h3>
                                <ul class="tSocials">
                                  <li><a href="https://www.facebook.com/kristiawanadi72" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="https://www.instagram.com/kristiawanadi72/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                              </div>
                              <!--End team desc-->

                            </div>
                            <!--End team-->


                            <!--Team-->
                            <div class="four columns teamSingle ">
                              <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/bimo.jpg"></div>

                              <!--Team desc-->
                              <div class="teamDesc">
                                <h3>Bimo Joko Sembodo<span>IT Security</span></h3>
                                <ul class="tSocials">
                                  <li><a href="https://www.facebook.com/asmi.kulo1" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                </ul>
                              </div>
                              <!--End team desc-->

                            </div>
                            <!--End team-->


                            <!--Team-->
                            <div class="four columns teamSingle">
                              <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/black.jpg"></div>

                              <!--Team desc-->
                              <div class="teamDesc">
                                <h3>Ikhwan Dirga Pratama<span>IT Security</span></h3>
                                <ul class="tSocials">
                                  <li><a href="https://www.facebook.com/ahgryd" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="https://www.instagram.com/dirga.id/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                              </div>
                              <!--End team desc-->

                            </div>
                            <!--End team-->

                            <!--Team-->
                            <div class="four columns teamSingle tCenter">
                              <div class="tImg"><img alt="" src="<?php echo base_url(); ?>assets/themes/webfront/images/teamImages/zurin.jpg"></div>

                              <!--Team desc-->
                              <div class="teamDesc">
                                <h3>Rino Ridlo Julianto<span>Web Developer</span></h3>
                                <ul class="tSocials">
                                  <li><a href="https://www.facebook.com/RynoVengeance" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="https://www.instagram.com/zurin_arctus" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                  <li><a href="https://www.twitter.com/@Ryno_Vengenz" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="https://t.me/ZurinArctus" target="_blank"><i class="fa fa-paper-plane"></i></a></li>
                                </ul>
                              </div>
                              <!--End team desc-->

                            </div>
                            <!--End team-->


                          </div>


              </div>
              <!--End container-->


                </div>
                <!--End team-->

  </section>
  <!--End about section-->
