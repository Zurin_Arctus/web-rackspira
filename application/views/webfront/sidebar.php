<div class="five columns sidebar">

  <!--Widget-->
  <div class="widget">
    <form action="<?php echo base_url(); ?>home/cari" class="searchForm" method="post">
         <input type="text" name="key" placeholder="Cari artikel">
         <!-- <input type="submit" class="submitSearch" value="Cari"> -->
         <button type="submit" name="cari" class="submitSearch"><i class="fa fa-search"></i></button>
       </form>
  </div>
  <!--End widget-->

    <!--Widget-->
    <div class="widget">
    <h2>Kategori</h2>
    <ul class="catList">
      <li><a href="<?php echo base_url(); ?>home/artikel"> Semua kategori <span class="countCat">(<?php echo count($this->front_model->get_data('tb_artikel')); ?>)</span></a></li>
      <?php foreach ($kategori as $key => $value) { ?>
      <li><a href="<?php echo base_url(); ?>home/artikel/<?php echo urlencode(base64_encode($value->id_kategori)); ?>"> <?php echo $value->judul_kategori; ?> <span class="countCat">(<?php echo count($this->front_model->get_where('tb_artikel', 'id_kategori', $value->id_kategori)) ?>)</span></a></li>
      <?php } ?>
    </ul>

    </div>
    <!--End widget-->

    <!--Widget-->
    <div class="widget">
    <h2>Arsip</h2>
      <ul class="catArchives">
      <?php
        foreach ($arsip as $key => $value) {
           $datetime = strtotime($value->tgl);
           $tday = date("F Y", $datetime);
           $count = $value->itemCount;
           $pisah = explode(" ", date("m Y", $datetime));
           $bln = $pisah[0];
           $thn = $pisah[1];
      ?>
      <li><a href="<?php echo base_url(); ?>home/arsip/<?php echo $bln."/".$thn; ?>"><?php echo $tday." (".$count.")" ?></a></li>
      <?php } ?>
    </ul>

    </div>
    <!--End widget-->

</div>
