<!--Single teaser -->

<section class="singleTeaser ofsTop">



    <!--Single teaser holder-->
  <div class="stHolder">


    <!--Images teaser single-->
    <div class="imgTS"></div>
    <!--End single teaser images-->

    <!--Container-->
    <div class="container clearfix">

      <!--Single teaser inner-->
      <div class="stInner ten columns margBottom margMTop ">

        <div class="stDetails">

          <div class="stTitle">

            <h1>Tentang Kami</h1>
            <p> Mengetahui tentang kami dan bidang yang kami miliki</p>

            </div>

        </div>


      </div>

      <!--End single teaser inner-->



      <div class="six columns margBottom margMTop">
      <a href="<?php echo base_url(); ?>home/karya" class="btn btnLaunch" >Lihat Karya Kami</a>
      </div>



      </div>
      <!--End container-->


  </div>
  <!--End single teaser holder-->


</section>
<!--End single teaser-->





<!--About section-->
  <section id="about">


    <!--Intro holder-->
    <div class="introHolder bgGrey margBottom">

    <div class="introBg bgGreyDark"></div>

    <!--Container-->
    <div class="container clearfix">


      <!--Intro-->
      <div class="seven columns intro">


        <!--Intro-->
        <div class="innerIntro tCenter">

        <div class="introTitle">
        <h1>Rack Spira</h1>
        <img src="<?php echo base_url(); ?>assets/themes/webfront/images/starL.png" alt="">
        </div>
        <p align="justify">
          Rack Spira adalah komunitas yang berdiri 20 November 2010 di Saraswangi sebelah kampus STMIK AKAKOM YOGYAKARTA
        </p>
        <!-- <br>
        <p align="left">Makna Logo Rack Spira:
        </p>
        <p align="justify">
          <span class="brand">1.</span> Anak kecil identik dengan kenakalan dan berimajinasi, jadi Rack Spira itu nakal dan liar (imajinasi) <br>
          <span class="brand">2.</span> Pakaian jawa, Rack Spira berdiri di Jawa dan itu menjadi  ciri khas dengan kesopanan <br>
          <span class="brand">3.</span> Acungan jempol, menandakan kualitas yang diakui <br>
          <span class="brand">4.</span> Warna coklat adalah warna tanah yang subur, ini menandakan Rack Spira adalah komunitas yang subur dan ada di bumi ini <br>
          <span class="brand">5.</span> Senyuman, tanda kalau Rack Spira adalah komunitas yang berdiri atas kekeluargaan.
        </p> -->
        <br>
        <p>
          "Jika kamu ingin menjadi seorang pemenang, maka rasakanlah gejolak kegilaan bersama kami
          karena kami benar-benar gila di jalan yang benar"
        </p>
        <p><span class="brand">~ Rack Edan Rack Spira ~</span></p>


            <ul class="introSocial">
                <li><a href="https://facebook.com/RackSpiraFans" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://instagram.com/RackSpira" target="_blank"><i class="fa fa-instagram"></i></a></li>
            </ul>

        </div>
        <!--End inner intro-->
      </div>
      <!--End intro-->



      <!--Feature-->
      <div class="nine columns features ">


        <!--Inner features-->
        <div class="featuresInner clearfix">

          <!--Feature single-->
          <div class="ft clearfix margHBottom">
          <div class="ftIco">
            <span class="ico"><i class="icon icon-lightbulb"></i></span>
          </div>

          <div class="ftDet">
            <h3>Kami merupakan wadah</h3>
            <p>Komunitas kami dapat menjadi wadah untuk menemukan, mengembangkan, maupun menyalurkan keminatan Anda
            dalam bidang teknologi dan informasi.  </p>
          </div>
        </div>
        <!--End feature single-->

        <!--Feature single-->
        <div class="ft clearfix margHBottom">
        <div class="ftIco">
          <span class="ico"><i class="icon icon-telescope"></i></span>
        </div>

        <div class="ftDet">
          <h3>Mengeksplorasi Hal Baru</h3>
          <p>Komunitas kami dapat menampung pengetahuan-pengetahuan baru baik secara teori maupun praktik
          dalam bidang teknologi informasi dan menangani pembelajarannya secara bersama-sama.  </p>
        </div>
      </div>
      <!--End feature single-->


      <!--Feature single-->
      <div class="ft clearfix margHBottom">
      <div class="ftIco">
        <span class="ico"><i class="icon  icon-layers"></i></span>
      </div>

      <div class="ftDet">
        <h3>Menghasilkan berbagai karya </h3>
        <p>Komunitas kami berorientasi pada hasil yang berupa karya sehingga dapat menghasilkan individu yang
        berkualitas dalam keilmuan maupun skill dalam bidang teknologi dan informasi  </p>
      </div>
    </div>
    <!--End feature single-->


        </div>
        <!--End inner features-->

      </div>
      <!--End features-->

    </div>
    <!--End container-->

    </div>
    <!--End intro holder-->


        <!--Process-->
      <div class="process tCenter ofsTop ofsBottom  ">


        <!--Container-->
        <div class="container clearfix">

          <div class="smallIntro">
            <p>Kami ingin berbagi bagaimana <span>proses</span>
            belajar kami dalam komunitas ini
            </p>
          </div>

          <!--Process inner-->
          <div class="processInner  ofsInTop clearfix">


            <!--Process single-->
            <div class="prc one-third column ">
              <div class="prcIco"><i class="icon icon-presentation"></i></div>
              <h3>Memicu</h3>
              <p>Kami akan mulai dengan memicu individu anggota kami untuk belajar terkait bidang minat yang
                dikehendakinya dalam langkah tertentu sesuai kemampuan yang dimiliki.</p>
            </div>
            <!--End process single-->




          <!--Process single-->
          <div class="prc one-third column ">
            <div class="prcIco"><i class="icon icon-beaker"></i></div>
            <h3>Desain / Rancangan</h3>
            <p>Apabila seorang individu dapat dikatan mampu dan telah mengetahui konsep secara luas mengenai keminatan
              yang dikehendakinya, maka akan beranjak ke proses desain atau pengembangan untuk menghasilkan karya
              terkait.</p>
          </div>
          <!--End process single-->


          <!--Process single-->
          <div class="prc one-third column ">
            <div class="prcIco"><i class="icon icon-genius"></i></div>
            <h3>Praktik / Pengembangan</h3>
            <p>Apabila desain atau rancangan telah selesai dibuat maka tahap terakhir adalah implementasi untuk mewujudkan
              rancangan tersebut menjadi karya yang dapat dimanfaatkan secara eksplisit.</p>
          </div>
          <!--End process single-->




          </div>
          <!--End process inner-->

        </div>
        <!--End container-->


      </div>

        <!--End process-->





  </section>
  <!--End about section-->
