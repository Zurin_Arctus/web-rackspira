<!--Single teaser -->
<?php if($this->uri->segment(3) == NULL) { ?>
<section class="singleTeaser ofsTop">



    <!--Single teaser holder-->
  <div class="stHolder">


    <!--Images teaser single-->
    <div class="imgTS"></div>
    <!--End single teaser images-->

    <!--Container-->
    <div class="container clearfix">

      <!--Single teaser inner-->
      <div class="stInner ten columns margBottom margMTop ">

        <div class="postContent">

          <div class="postTitleL">

            <h1>Tidak ada yang perlu ditampilkan</h1>

            </div>


        </div>


      </div>

      <!--End single teaser inner-->



      <div class="six columns margBottom margMTop">
      <a href="<?php echo base_url(); ?>home/artikel" class="btn btnLaunch" >Kembali ke daftar artikel</a>
      </div>



      </div>
      <!--End container-->


  </div>
  <!--End single teaser holder-->


</section>

<?php } else { ?>
<section class="singleTeaser ofsTop">



    <!--Single teaser holder-->
  <div class="stHolder">


    <!--Images teaser single-->
    <div class="imgTS"></div>
    <!--End single teaser images-->

    <!--Container-->
    <div class="container clearfix">

      <!--Single teaser inner-->
      <div class="stInner ten columns margBottom margMTop ">

        <div class="postContent">

          <div class="postTitleL">

            <h1><?php echo $judul; ?></h1>
            <div class="postMeta">
              <span class="metaAuthor"><a href="#">Admin - </a></span>
              <span class="metaCategory"><a href="#"><?php echo $judul_kategori; ?> - </a></span>
              <span class="metaDate"><a href="#"><?php echo $tanggal; ?> - </a></span>
              <span class="metaComments"><a href="#"><?php echo $jumlah_komentar; ?> Komentar</a></span>
             </div>

            </div>


        </div>


      </div>

      <!--End single teaser inner-->



      <div class="six columns margBottom margMTop">
      <a href="<?php echo base_url(); ?>home/artikel" class="btn btnLaunch" >Kembali ke daftar artikel</a>
      </div>



      </div>
      <!--End container-->


  </div>
  <!--End single teaser holder-->


</section>
<!--End single teaser-->


  <!--BLog single section-->
  <section id="blogSingle" class="blogSingle  margHTop margHBottom">

      <!--Container-->
      <div class="container clearfix">
      <div class="eleven columns">

        <!--Post Single-->
        <div class="postSingle">

    <!--Post content-->
    <div class="postContent">

        <!--Post image-->
        <div class="postMedia postSliderLarge flexslider">
        <ul class="slides" id="slides">
          <?php foreach ($gambar as $key => $value) {?>
          <li>
            <a data-fancybox class="slide" href="<?php echo base_url(); ?>assets/uploads/artikel/<?php echo $value->gambar; ?>">
              <img alt="" src="<?php echo base_url(); ?>assets/uploads/artikel/<?php echo $value->gambar; ?>" class="img-responsive">
            </a>
          </li>
          <?php } ?>
        </ul>

        </div>
      <!--End post image-->

      <p align="justify">
        <?php echo $isi; ?>
      </p>

      <div class="tagsSingle clearfix">
  		  <h4><i class="icon-tag-1"></i>Kategori :</h4>
  				<ul class="tagsListSingle">
  						<li><a href="<?php echo base_url(); ?>home/artikel/<?php echo urlencode(base64_encode($id_kategori)); ?>"><?php echo $judul_kategori; ?></a></li>
  				</ul>
  		</div>



    </div>
    <!--End post content-->

    </div>
    <!--End post single-->





    <!--Comments-->
    <div class="comments">
      <h2><?php echo $jumlah_komentar; ?> Komentar</h2>
      <?php foreach ($komentar as $key => $nil) { ?>
      <!--Entries container-->
      <div class="entriesContainer">

        <!--Comments and replys-->

        <ul class="comments clearfix">
        <li>
        <div class="comment">

        <div class="img">
          <!-- <i class="fa fa-user"></i> -->
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/favicon.ico" alt="">
        </div>
        <div class="commentContent">
          <div class="commentsInfo">
            <div class="author"><a href="#"><?php echo $nil->nama; ?></a></div>
            <div class="date">
              <a href="#">
                <?php
                    $pisah =  explode(" ", $nil->tgl);
                    $tgl = date_create($pisah[0]);
                    $jam = date_create($pisah[1]);
                    echo "Diposkan tanggal ".date_format($tgl, "j M, Y")." pukul ".date_format($jam,"H:i");
                 ?>
              </a>
            </div>
          </div>
          <p class="expert">
            <?php echo $nil->isi_komentar; ?>
          </p>
        </div>
      </div>
    </div>
    <?php } ?>

      <!--End  entries container -->

    </div>
    <!--End comments-->



    <!--Respond-->
    <div class="respond margTop">
    <h2>Berikan Komentar</h2>

    <!--Reply form-->
    <div class="replyForm">
      <form method="POST" action="<?php echo base_url(); ?>home/komen">

        <!--Input columns-->
        <div class="inputColumns clearfix">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input type="hidden" name="url" value="<?php echo $url; ?>">
        <div class="column1">
          <div class="columnInner">
        <input type="text" placeholder="Nama lengkap *" value="" id="nama" name="nama" required>
        </div>
        </div>

        <!--Column-->
        <div class="column2">
          <div class="columnInner">
        <input type="text" placeholder="Email *" value="" id="email" name="email" required>
        </div>
        </div>
        <!--End column-->

        </div>
        <!--End input columns-->

      <textarea name="isi" id="isi"  placeholder="Isi komentar *" cols="45" rows="10" required></textarea>
      <button type="submit" name="kirim" class="btn" id="submit">Kirim Komentar</button>
      </form>

    </div>
    <!--End reply form-->


    </div>
    <!--End respond-->


  </div>

  <?php include "sidebar.php" ?>

</section>
<!--End blog single section-->
<?php } ?>
