<!--Single teaser -->

<section class="singleTeaser ofsTop">



    <!--Single teaser holder-->
  <div class="stHolder">


    <!--Images teaser single-->
    <div class="imgTS"></div>
    <!--End single teaser images-->

    <!--Container-->
    <div class="container clearfix">

      <!--Single teaser inner-->
      <div class="stInner ten columns margBottom margMTop ">

        <div class="postContent">

          <div class="postTitleL">

            <h1>Artikel </h1>
            <p>Beberapa artikel yang mungkin menarik bagi Anda</p>

            </div>


        </div>


      </div>

      <!--End single teaser inner-->



      <div class="six columns margBottom margMTop">


        <!--Pagination-->
        <div class="pagination">
          <?php echo $pagination; ?>
        </div>

        <!--End pagination-->


      </div>



      </div>
      <!--End container-->


  </div>
  <!--End single teaser holder-->


</section>
<!--End single teaser-->





  <!--BLog full section-->
  <section id="blogFull" class="blogFull margHTop margHBottom">

      <!--Container-->
      <div class="container clearfix">

      <div class="eleven columns">
<?php if($jumlah==0) { ?>
<div class="postLarge">
  <div class="postContent">
    <div class="postTitle">
      <h1 align="center">Maaf, data kosong!</h1>
    </div>
  </div>
</div>

<?php
  } else {
  foreach ($artikel as $key => $value) {
?>
      <!--Post large-->
      <div class="postLarge">

  <!--Post content-->
  <div class="postContent">

    <div class="postTitle">
    <h1><a href="<?php echo base_url(); ?>home/artikel_detail/<?php echo $value->url; ?>"><?php echo $value->judul_artikel; ?></a></h1>

    <!--Post meta-->
    <div class="postMeta">
      <span class="metaAuthor"><a href="#">Admin - </a></span>
      <span class="metaCategory"><a href="#"><?php echo $value->judul_kategori; ?> - </a></span>
      <span class="metaDate">
        <a href="#">
          <?php
              $tanggal = date_create($value->tgl);
              echo date_format($tanggal, "j M Y");
           ?> -
        </a>
      </span>
      <span class="metaComments"><a href="<?php echo base_url(); ?>home/artikel_detail/<?php echo $value->url; ?>">
        <?php echo count($this->front_model->get_where('tb_komentar', 'id_artikel', $value->id_artikel)); ?> Komentar
      </a></span>
     </div>
    <!--End post meta-->

    </div>

      <!--Post image-->
      <div class="postMedia postSliderLarge flexslider">
        <ul class="slides">
        <?php
          $gambar = $this->front_model->get_where('tb_gambar_artikel', 'id_artikel', $value->id_artikel);
          foreach ($gambar as $key => $gbr)
        {?>
        <li><a href="<?php echo base_url(); ?>home/artikel_detail/<?php echo $value->url; ?>"><img src="<?php echo base_url(); ?>assets/uploads/artikel/<?php echo $gbr->gambar; ?>" alt=""/></a></li>
        <?php } ?>
        </ul>
      </div>
    <!--End post image-->

    <p align="justify">
      <?php
        $strip = strip_tags(stripcslashes($value->isi_artikel), '<a>');
        echo substr($strip, 0, 300);
        if (strlen($strip)>300) echo "....";
      ?>
    </p>

          <div class=" more">
          <a class="btn more" href="<?php echo base_url(); ?>home/artikel_detail/<?php echo $value->url; ?>">Baca Selengkapnya</a>
          </div>


  </div>
  <!--End post content-->

  </div>
  <!--End post large-->
  <?php } } ?>
  <section class="teaserB ofsTMedium ofsBMedium" style="background: #9d9d9d;">


    <div class="container clearfix">

      <div class="ten columns teaserMsg">
        <!--Pagination-->
        <div class="pagination">
          <?php echo $pagination; ?>
        </div>

        <!--End pagination-->
      </div>
    </div>
  </section>

  </div>

  <?php include "sidebar.php" ?>


      </div>
      <!--End container-->
  </section>
  <!--End blog full section-->
