<!--Single teaser -->

<section class="singleTeaser ofsTop">



    <!--Single teaser holder-->
  <div class="stHolder">


    <!--Images teaser single-->
    <div class="imgTS"></div>
    <!--End single teaser images-->

    <!--Container-->
    <div class="container clearfix">

      <!--Single teaser inner-->
      <div class="stInner ten columns margBottom margMTop ">

        <div class="stDetails">

          <div class="stTitle">

            <h1>Karya Kami</h1>
            <p>Karya yang telah kami buat</p>

            </div>

        </div>


      </div>

      <!--End single teaser inner-->



      <div class="six columns margBottom margMTop">
      &nbsp;
      </div>



      </div>
      <!--End container-->


  </div>
  <!--End single teaser holder-->


</section>
<!--End single teaser-->





  <!--Portfolio section-->
  <section id="portfolio">






      <!--Container-->
      <div class="container clearfix">

      <!-- Filter nav -->
      <div class="filterNav ofsInTop ofsInBottom">
        <ul id="category" class="filter">
          <li class="all current"><a href="#">Semua</a></li>
          <?php foreach ($kategori_karya as $key => $value) { ?>
          <li class="<?php echo $value->id_kategori_karya; ?>"><a href="#"> <?php echo $value->judul_kategori_karya; ?></a></li>
          <?php } ?>
        </ul>
        </div>
      <!-- End filter nav -->


      </div>
      <!--End container-->



      <!--Container-->
      <div class="container clearfix">

          <!-- Works list -->
          <div class=" works clearfix ">
            <!--Portfolio-->
              <ul class="portfolio clearfix tCenter">


        <li class="item one-third column " data-id="id-1" data-type="catWeb">
          <div class="itemDesc">

            <div class="itemDescInner">
            <h3>Testy Raspberry<span> - web design</span></h3>
            <div class=" itemBtn ">
            <a  href="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w1.jpg" class=" folio"><i class="fa fa-expand"></i></a>
            <a href="project.html"><i class="fa fa-link"></i></a>
            </div>
            </div>
          </div>
            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w1.jpg" alt="">
        </li>



        <li class="item one-third column " data-id="id-2" data-type="catWeb">
          <div class="itemDesc">
              <div class="itemDescInner">
              <h3>Summer Time<span> - motion graphic</span></h3>
              <div class=" itemBtn ">
                <a  class="popup-vimeo" href="http://player.vimeo.com/video/51173359"><i class="fa fa-expand"></i></a>
                <a href="project_video.html"><i class="fa fa-link"></i></a>
              </div>
              </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w2.jpg" alt="">
        </li>



        <li class="item one-third column " data-id="id-3" data-type="catGraphic">
          <div class="itemDesc">
              <div class="itemDescInner">
              <h3>Skyscraper<span> - graphic design</span></h3>
              <div class=" itemBtn ">
                <a  href="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w3.jpg" class=" folio"><i class="fa fa-expand"></i></a>
                <a href="project.html"><i class="fa fa-link"></i></a>
              </div>
              </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w3.jpg" alt="">
        </li>

        <li class="item one-third column " data-id="id-4" data-type="catGraphic">
          <div class="itemDesc">
              <div class="itemDescInner">
              <h3>Summer Time<span> - graphic design</span></h3>
              <div class=" itemBtn ">
                <a  href="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w4.jpg" class=" folio"><i class="fa fa-expand"></i></a>
                <a href="project.html" ><i class="fa fa-link"></i></a>
              </div>
              </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w4.jpg" alt="">
        </li>

        <li  class="item one-third column " data-id="id-5" data-type="catGraphic">
          <div class="itemDesc">
            <div class="itemDescInner">
            <h3>Great Restaurant<span> - web design</span></h3>
            <div class=" itemBtn ">
              <a  href="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w5.jpg" class=" folio"><i class="fa fa-expand"></i></a>
              <a href="project.html" ><i class="fa fa-link"></i></a>
            </div>
            </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w5.jpg" alt="">

        </li>


        <li class="item one-third column " data-id="id-6" data-type="catMotion">
          <div class="itemDesc">
            <div class="itemDescInner">
            <h3>Runing Road<span> - motion design</span></h3>
            <div class=" itemBtn ">
              <a  class="popup-vimeo" href="http://player.vimeo.com/video/51173359"><i class="fa fa-expand"></i></a>
              <a href="project_video.html"><i class="fa fa-link"></i></a>
            </div>
            </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w6.jpg" alt="">

        </li>
        <li class="item one-third column" data-id="id-7" data-type="catMotion">
          <div class="itemDesc">
            <div class="itemDescInner">
            <h3>Night Light<span> - web design</span></h3>
            <div class=" itemBtn ">
              <a  class="popup-vimeo" href="http://player.vimeo.com/video/51173359"><i class="fa fa-expand"></i></a>
              <a href="project_video.html" ><i class="fa fa-link"></i></a>
            </div>
            </div>
          </div>
          <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w7.jpg" alt="">

        </li>

          <li class="item one-third column " data-id="id-8" data-type="catMotion">
            <div class="itemDesc">
              <div class="itemDescInner">
              <h3>Cheat Day<span> - motion graphic</span></h3>
              <div class=" itemBtn ">
                <a  class="popup-vimeo" href="http://player.vimeo.com/video/51173359"><i class="fa fa-expand"></i></a>
                <a href="project_video.html" ><i class="fa fa-link"></i></a>
              </div>
              </div>
            </div>
            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w8.jpg" alt="">

          </li>
          <li class="item  one-third column " data-id="id-9" data-type="catMotion">
            <div class="itemDesc">
              <div class="itemDescInner">
              <h3>Speed Road<span> - web design</span></h3>
              <div class=" itemBtn ">
                <a  class="popup-vimeo" href="http://player.vimeo.com/video/51173359"><i class="fa fa-expand"></i></a>
                <a href="project_video.html"><i class="fa fa-link"></i></a>
              </div>
              </div>
            </div>
            <img src="<?php echo base_url(); ?>assets/themes/webfront/images/portfolioImages/w9.jpg" alt="">

          </li>

        </ul>
        <!--End portfolio-->
        </div>
        <!-- End works list -->

        </div>
        <!--End container-->


    </section>
    <!--End portfolio section-->
