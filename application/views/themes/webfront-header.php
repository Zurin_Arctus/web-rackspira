<!--Header-->
  <header id="header" >

    <!--Main header-->
      <div class="mainHeader default">


        <!--Container-->
      <div class="container clearfix">
      <div class="two columns">
        <!--Logo-->
        <div class="logo">
        <a  class="scroll" href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>assets/themes/webfront/images/rackspira.png" alt="logo" style="max-width:none;"></a>
        </div>
        <!--End logo-->
      </div>


        <div class="fourteen columns">
          <a href="#" class="mobileBtn" ><i class="fa fa-bars"></i></a>
            <!--Navigation-->
              <nav class="mainNav" >


              <ul>
                <li><a  href="<?php echo base_url(); ?>home">Beranda</a></li>
                <li><a  href="<?php echo base_url(); ?>home/artikel">Artikel</a>
                    <!--Submenu-->
                    <ul class="dropDown">
                      <?php foreach ($kategori as $key => $category) { ?>
                      <li><a href="<?php echo base_url(); ?>home/artikel/<?php echo urlencode(base64_encode($category->id_kategori)); ?>"><?php echo $category->judul_kategori; ?></a></li>
                      <?php } ?>
                    </ul>
                    <!--End submenu-->

                  </li>
                  <li><a  href="<?php echo base_url(); ?>home/karya">Karya</a></li>
                <li><a  href="<?php echo base_url(); ?>home/about">Tentang Kami</a></li>

              </ul>

              </nav>
            <!--End navigation-->

        </div>
      <!--End container-->




      </div>
    <!--End main header-->
      </div>

  </header>
  <!--End header-->
