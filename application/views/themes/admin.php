<!doctype html>
<html lang="en">

<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Nov 2016 04:33:53 GMT -->
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/themes/admin/assets/img/logo.png" />
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/themes/admin/assets/img/favicon.ico" />

	<title>Admin Rack Spira</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Canonical SEO -->
    <link rel="canonical" href="http://www.creative-tim.com/product/material-dashboard-pro"/>



  <?php
  /** -- Copy from here -- */
  if(!empty($meta))
  foreach($meta as $name=>$content){
    echo "\n\t\t";
    ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
       }
  echo "\n";

  if(!empty($canonical))
  {
    echo "\n\t\t";
    ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

  }
  echo "\n\t";

  foreach($css as $file){
    echo "\n\t\t";
    ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
  } echo "\n\t";

  /** -- to here -- */
  ?>

</head>

<body>
	<div class="wrapper">
	    <div class="sidebar" data-active-color="red" data-background-color="black" data-image="<?php echo base_url(); ?>assets/themes/admin/assets/img/sidebar-1.jpg">
	    <!--
	        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
	        Tip 2: you can also add an image using data-image tag
	        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
	    -->

		    <div class="logo">
		        <a href="<?php echo base_url(); ?>arthur" class="simple-text">
		            Admin Rack Spira
		        </a>
		    </div>

		    <div class="logo logo-mini">
				<a href="<?php echo base_url(); ?>arthur" class="simple-text">
					ARS
				</a>
			</div>

    <?php include "admin-sidebar.php"; ?>

		</div>


	    <div class="main-panel">
			<?php include "admin-navbar.php"; ?>
			<div class="content">
			  <div class="container-fluid">
      		<?php echo $output;?>
				</div>
			</div>
      <?php include "admin-footer.php"; ?>

		</div>
	</div>

	<?php include "admin-widget.php"; ?>
</body>
  <?php
    foreach($js as $file){
        echo "\n\t\t";
        ?><script src="<?php echo $file; ?>"></script><?php
    } echo "\n\t";
  ?>
	<!--  Google Maps Plugin    -->
	<script src="https://maps.googleapis.com/maps/api/js"></script>


	<script type="text/javascript">
	    $(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
	        demo.initDashboardPageCharts();

			demo.initVectorMap();
	    });
	</script>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Nov 2016 04:37:04 GMT -->
</html>
