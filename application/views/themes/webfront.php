<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Benaissa Ghrib" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title><?php echo $halaman; ?> | Rack Spira</title>

<!--Stylesheet-->

<!--[if IE 7]>
<link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->

<link href="<?php echo base_url(); ?>assets/themes/webfront/css/font.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/etlinefont.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/skeleton.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/main.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/webfront/css/magnific-popup.css" rel="stylesheet"  />

<?php
/** -- Copy from here -- */
if(!empty($meta))
foreach($meta as $name=>$content){
  echo "\n\t\t";
  ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
     }
echo "\n";

if(!empty($canonical))
{
  echo "\n\t\t";
  ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

}
echo "\n\t";

foreach($css as $file){
  echo "\n\t\t";
  ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
} echo "\n\t";

/** -- to here -- */
?>

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--[if lt IE 8]>
<style>
/* For IE < 8 (trigger hasLayout) */
.clearfix {
    zoom:1;
}
</style>
<![endif]-->
<!-- Favicons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/webfront/images/favicon.ico">

</head>

<body>

	<!-- Preloader -->
	<div id="loader">
			<!-- Preloader inner -->
	  <div id="loaderInner">

		<!-- Loader bars -->
			<div class="loaderBars">
				<span class="bar1 bar"></span>
				<span class="bar2 bar"></span>
				<span class="bar3 bar"></span>
			</div>
			<!-- End loader bars -->

	  </div>
	<!-- End preloader inner -->
	</div>
	<!-- End preloader -->






<!--Wrapper-->
<div id="wrapper">


	<?php include "webfront-header.php"; ?>


  <?php echo $output;?>


	<?php include "webfront-footer.php" ?>

</div>
<!--End wrapper-->


<!--Javascript-->
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery-migrate-1.2.1.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery.quicksand.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/spectragram.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/themes/webfront/js/script.js" type="text/javascript"></script>
<?php
  foreach($js as $file){
      echo "\n\t\t";
      ?><script src="<?php echo $file; ?>"></script><?php
  } echo "\n\t";
?>
<script type="text/javascript" class="init">
  $(document).ready(function() {
  // $(".flexslider li:not(.cloned) .slide").fancybox({
  //   prevEffect		: 'none',
  //   nextEffect		: 'none',
  //   arrows : false,
  //   closeBtn		: false
  // });
  $('[data-fancybox]').fancybox({
    toolbar  : false,
    smallBtn : true,
    iframe : {
      preload : false
    }
  })
  });
</script>
<!-- Google analytics -->


<!-- End google analytics -->


</body>
</html>
