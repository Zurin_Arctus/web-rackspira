<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="<?php echo base_url(); ?>home" target="_blank">
                       Web Front
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> Rack Spira
        </p>
    </div>
</footer>
