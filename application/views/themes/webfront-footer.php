<!--Teaser bottom-->
<section class="teaserB ofsTMedium ofsBMedium">


  <div class="container clearfix">

    <div class="twelve columns teaserMsg">
      <h1>Rack Edan, Rack Spira</h1>
    </div>


    <div class="four columns buyLink">

      <img src="<?php echo base_url(); ?>assets/themes/webfront/images/logo.png" alt="logo">

    </div>

  </div>


</section>
<!--End teaser bottom-->

<!--Footer-->
<footer id="footer" class="footer ">



  <!--Footer top-->
  <div class="footerTop ofsTop ofsBMedium">
      <!--Container-->
      <div class="container clearfix">

          <!--Footer top inner-->
          <div class="ftInner clearfix">

              <!--Widget footer-->
              <div class="one-third column widgetFooter">
              <h3>Tentang Kami</h3>
              <div class="introWidget">
                <p align="justify">
                  Rack Spira adalah komunitas yang berdiri 20 November 2010 di Saraswangi sebelah kampus STMIK AKAKOM YOGYAKARTA
                </p>
              </div>

              </div>
              <!--End widget footer-->



            <!--Widget footer-->
            <div class="one-third column widgetFooter">
            <h3>instagram</h3>
            <ul class="instaFeed" id="instafeed"></ul>

            </div>
            <!--End widget footer-->



              <!--Widget footer-->
              <div class="one-third column widgetFooter">
              <h3>Kontak</h3>
              <div class="introWidget">
                <p align="justify">Apabila Anda memiliki pertanyaan atau butuh bantuan silakan hubungi kami melalui kontak yang tertera di bawah ini:</p>
              </div>
              <ul class="widgetInfo">
                <li><span>No. HP</span> : +62-877-3921-1471</li>
                <li><span>Grup Telegram</span> : t.me/rackspira</li>
                <li><span>Facebook</span> : facebook.com/RackSpiraFans</li>
                <li><span>Instagram</span> : instagram.com/RackSpira</li>
              </ul>

              </div>
              <!--End widget footer-->




          </div>
          <!--End footer top inner-->

      </div>
        <!--End container-->

  </div>
  <!--End footer top-->




<!--Footer bottom-->
<div class="footerBottom ofsTSmall ofsBSmall">
    <!--Container-->
    <div class="container clearfix">


        <div class="eight columns ">
        <!--Footer bottom inner-->
        <div class="fbInner clearfix">

        <p>&copy; 2017 <span class="brand">Rack Spira</span>.</p>

        </div>

        <!--End footer bottom inner-->

        </div>



      <div class="eight columns ">

          <ul class="socialsFooter ">



            <li><a href="https://www.facebook.com/RackSpiraFans/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.instagram.com/RackSpira/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://t.me/rackspira" target="_blank"><i class="fa fa-paper-plane"></i></a></li>

          </ul>
      </div>


</div>
  <!--End container-->
</div>
  <!--End footer bottom-->


</footer>
<!--End footer-->
