<div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="<?php echo base_url(); ?>assets/themes/admin/assets/img/avatar.jpg" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <?php echo $nama; ?>
                <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li><a href="#">Setting Akun Admin</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php $hal=$this->uri->segment(2); ?>
    <ul class="nav">
        <li class="<?php if($hal==NULL) echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>arthur">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>

        <li class="<?php if($hal=='artikel_tambah') echo 'active'; ?>">
            <a data-toggle="collapse" href="#artikel" <?php if($hal=='artikel_tambah') { ?> aria-expanded="true" <?php } ?>>
                <i class="material-icons">book</i>
                <p>Artikel Area
                   <b class="caret"></b>
                </p>
            </a>

            <div class="collapse <?php if($hal=='artikel_tambah') echo 'in'; ?>" id="artikel">
                <ul class="nav">
                    <li class="<?php if($hal=='artikel_tambah') echo 'active'; ?>">
                        <a href="<?php echo base_url() ?>arthur/artikel_tambah">Tambah Artikel</a>
                    </li>
                    <li>
                        <a href="pages/timeline.html">Daftar Artikel</a>
                    </li>
                    <li>
                        <a href="pages/timeline.html">Gambar Artikel</a>
                    </li>
                    <li>
                        <a href="pages/login.html">Kategori Artikel</a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <a data-toggle="collapse" href="#karya">
                <i class="material-icons">filter_vintage</i>
                <p>Karya Area
                   <b class="caret"></b>
                </p>
            </a>

            <div class="collapse" id="karya">
                <ul class="nav">
                    <li>
                        <a href="pages/pricing.html">Tambah Karya</a>
                    </li>
                    <li>
                        <a href="pages/timeline.html">Daftar Karya</a>
                    </li>
                    <li>
                        <a href="pages/login.html">Kategori Karya</a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <a href="#logout" onclick="demo.showSwal('logout')">
                <i class="material-icons">exit_to_app</i>
                <p>Log Out</p>
            </a>
        </li>
    </ul>
</div>
