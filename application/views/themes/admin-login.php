<!doctype html>
<html lang="en">

<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/pages/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Nov 2016 04:37:18 GMT -->
<head>
  <meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/themes/admin/assets/img/logo.png" />
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/themes/admin/assets/img/favicon.ico" />

	<title>Admin Rack Spira</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Canonical SEO -->
    <link rel="canonical" href="http://www.creative-tim.com/product/material-dashboard-pro"/>

    <?php
    /** -- Copy from here -- */
    if(!empty($meta))
    foreach($meta as $name=>$content){
      echo "\n\t\t";
      ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
         }
    echo "\n";

    if(!empty($canonical))
    {
      echo "\n\t\t";
      ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

    }
    echo "\n\t";

    foreach($css as $file){
      echo "\n\t\t";
      ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
    } echo "\n\t";

    /** -- to here -- */
    ?>
</head>
<?php
  $message=$this->session->flashdata('login_error');
?>
<body  <?php if(!$message==""){ ?>onload="demo.showSwal('login-gagal')"<?php } ?> data-message="<?php echo $message; ?>">
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="../dashboard.html">Admin Rack Spira Login</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url(); ?>home">
                            <i class="material-icons">dashboard</i>
                            Halaman Depan
                        </a>
                    </li>
                    <li class= "active">
                        <a href="#">
                            <i class="material-icons">fingerprint</i>
                            Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper wrapper-full-page">
        <div class="full-page register-page" filter-color="black" data-image="<?php echo base_url(); ?>assets/themes/admin/assets/img/register.jpg">
            <div class="container">
                <?php echo $output;?>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>

                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script> Rack Spira
                    </p>
                </div>
            </footer>
        </div>
    </div>
    <?php include "admin-widget.php" ?>
</body>

    <?php
      foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
      } echo "\n\t";
    ?>

    <script type="text/javascript">
        $().ready(function(){
            demo.checkFullPageBackgroundImage();

            setTimeout(function(){
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
    </script>
    <script type="text/javascript">

  		function setFormValidation(id){
  			$(id).validate({
  				errorPlacement: function(error, element) {
  		            $(element).parent('div').addClass('has-error');
  		         }
  			});
  		}

  		$(document).ready(function(){
  			setFormValidation('#LoginValidation');
  		});

  	</script>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/pages/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Nov 2016 04:37:51 GMT -->
</html>
