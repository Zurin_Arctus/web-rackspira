<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arthur extends Auth_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->library('slug');
		$this->load->model('front_model');
		$sesi = $this->session->userdata("logged_in");
		$this->user = $this->front_model->get_where('tb_admin', 'id_user', $sesi['id_user']);
		$this->_init();
	}

	private function _init()
	{
		$this->output->set_template('admin');
		// <!-- Bootstrap core CSS     -->
		$this->load->css('assets/themes/admin/assets/css/bootstrap.min.css');
		// <!--  Material Dashboard CSS  -->
		$this->load->css('assets/themes/admin/assets/css/material-dashboard.css');
		// <!--  CSS for Demo Purpose, don't include it in your project  -->
		$this->load->css('assets/themes/admin/assets/css/demo.css');
		// <!--  Fonts and icons  -->
		$this->load->css('assets/themes/admin/assets/css/font-awesome.min.css');
		$this->load->css('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons');
		// <!--   Core JS Files   -->
		$this->load->js('assets/themes/admin/assets/js/jquery-3.1.1.min.js');
		$this->load->js('assets/themes/admin/assets/js/jquery-ui.min.js');
		$this->load->js('assets/themes/admin/assets/js/bootstrap.min.js');
		$this->load->js('assets/themes/admin/assets/js/material.min.js');
		$this->load->js('assets/themes/admin/assets/js/perfect-scrollbar.jquery.min.js');
		// <!-- Forms Validations Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.validate.min.js');
		// <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
		// $this->load->js('assets/themes/admin/assets/js/moment.min.js');
		// <!--  Charts Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/chartist.min.js');
		// <!--   Sharrre Library    -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.sharrre.js');
		// <!--  Plugin for the Wizard -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.bootstrap-wizard.js');
		// <!--  Notifications Plugin    -->
		// $this->load->js('assets/themes/admin/assets/js/bootstrap-notify.js');
		// <!-- DateTimePicker Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/bootstrap-datetimepicker.js');
		// <!-- Vector Map plugin -->
		// $this->load->js('assets/themes/admin/assets/js/jquery-jvectormap.js');
		// <!-- Sliders Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/nouislider.min.js');
		// <!-- Select Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.dropdown.js');
		// <!--  DataTables.net Plugin    -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.datatables.js');
		// <!-- Sweet Alert 2 plugin -->
		$this->load->js('assets/themes/admin/assets/js/sweetalert2.js');
		// <!--  Full Calendar Plugin    -->
		// $this->load->js('assets/themes/admin/assets/js/fullcalendar.min.js');
		// <!-- TagsInput Plugin -->
		// $this->load->js('assets/themes/admin/assets/js/jquery.tagsinput.js');
		// <!-- Material Dashboard javascript methods -->
		$this->load->js('assets/themes/admin/assets/js/material-dashboard.js');
		// <!-- Material Dashboard DEMO methods, don't include it in your project! -->
		$this->load->js('assets/themes/admin/assets/js/demo.js');
	}

	public function index()
	{
		$data = array(
			'nama' => $this->user[0]->nama_lengkap,
			'brand' => "Dashboard",
			'artikel' => count($this->front_model->get_data('tb_artikel')),
			'kategori' => count($this->front_model->get_data('tb_kategori')),
			'karya' => count($this->front_model->get_data('tb_karya')),
			'kategori_karya' => count($this->front_model->get_data('tb_kategori_karya'))
		);
		$this->load->view('admin/dashboard', $data);
	}

	public function artikel_tambah(){
		$data = array(
			'nama' => $this->user[0]->nama_lengkap,
			'brand' => "Tambah Artikel",
			'kategori' => $this->front_model->get_data_order('tb_kategori', 'judul_kategori', 'ASC')
		);
		$this->load->view('admin/artikel-tambah', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'lancelot');
	}
}
