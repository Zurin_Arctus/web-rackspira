<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('encryption');
		$this->load->library('pagination');
		$this->load->model('front_model');
		$this->_init();
		$this->kategori = $this->front_model->get_data_order('tb_kategori', 'judul_kategori', 'asc');
		$this->arsip = $this->db->query("SELECT *,COUNT(id_artikel) AS itemCount FROM tb_artikel GROUP BY DATE_FORMAT(tgl, '%Y-%m') DESC")->result();
	}

	private function _init()
	{
		$this->output->set_template('webfront');
		$this->load->css('assets/themes/webfront/css/owl.carousel.css');
		$this->load->css('assets/themes/webfront/css/owl.theme.css');
		$this->load->css('assets/themes/webfront/css/flexslider.css');
    $this->load->js('assets/themes/webfront/js/jquery.flexslider-min.js');
    $this->load->js('assets/themes/webfront/js/jquery.magnific-popup.js');
    $this->load->js('assets/themes/webfront/js/jquery.appear.min.js');
    $this->load->js('assets/themes/webfront/js/owl.carousel.min.js');
		$this->load->js('assets/themes/webfront/js/jquery.tweet.js');
		$this->output->set_meta('title', 'Rack Spira');
	}

	public function index()
	{
    $this->load->js('assets/themes/webfront/js/Placeholders.min.js');
    $this->load->js('assets/themes/webfront/js/jquery.timer.js');
		$this->output->set_meta('description', 'SITUS RESMI DARI KOMUNITAS RACK SPIRA. KAMI MERUPAKAN KOMUNITAS YANG MEWADAHI KEGIATAN SHARING ATAU BELAJAR BERSAMA DALAM BIDANG TEKNOLOGI DAN INFORMASI.');

		$data = array(
			'halaman' => "Beranda",
			'kategori' => $this->kategori
		);
		$this->load->view('webfront/home', $data);
	}

	public function about()
	{
		$this->output->set_meta('description', 'Rack Spira adalah komunitas yang berdiri 20 November 2010 di Saraswangi sebelah kampus STMIK AKAKOM YOGYAKARTA');
		$data = array(
			'halaman' => 'Tentang Kami',
			'kategori' => $this->kategori
		);
		$this->load->view('webfront/about', $data);
	}

	public function karya()
	{
		$this->output->set_meta('description', 'Karya yang telah kami buat');
		$data = array(
			'halaman' => 'Karya',
			'kategori' => $this->kategori,
			'kategori_karya' => $this->front_model->get_data('tb_kategori_karya')
		);
		$this->load->view('webfront/karya', $data);
	}

	public function artikel(){
		$this->output->set_meta('description', 'Artikel yang mungkin menarik bagi Anda');
		$nol = urlencode(base64_encode(0));
		$kat = $nol;
		$artikel = null;

		if ($this->uri->segment(3) != null) {
			$kat = $this->uri->segment(3);
		}

		//pagination settings
        $config['base_url'] = base_url().'home/artikel/'.$kat.'/';
        if ($kat == $nol) {
        	$config['total_rows'] = $this->db->count_all('tb_artikel');
        }else {
					$id = base64_decode(urldecode($kat));
        	$config['total_rows'] = $this->front_model->count_where('tb_artikel', 'id_kategori',$id);
        }

        $config['per_page'] = "4";
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="#" class="selected">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
				$data['hal'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

			if ($kat == $nol) {
				$artikel = $this->front_model->get_data_artikel($config["per_page"], $data['hal']);
			}else {
				$artikel = $this->front_model->get_data_artikel_per_kategori($id, $config["per_page"], $data['hal']);
			}

		$data = array(
			'halaman' => 'Artikel',
			'kategori' => $this->kategori,
			'arsip' => $this->arsip,
			'pagination' => $this->pagination->create_links(),
			'artikel' => $artikel,
			'jumlah' => count($artikel)
		);
		$this->load->view('webfront/artikel', $data);
	}

	public function artikel_detail()
	{
		$this->load->css('assets/themes/webfront/css/jquery.fancybox.css');
    $this->load->js('assets/themes/webfront/js/jquery.fancybox.js');

		$url = $this->uri->segment(3);
		if($url!=null){
			$artikel = $this->front_model->get_artikel_single($url);
			$create_tgl = date_create($artikel[0]->tgl);
			$tgl = date_format($create_tgl, "j M Y");
			$komentar = $this->front_model->get_where('tb_komentar', 'id_artikel', $artikel[0]->id_artikel);
			$data = array(
				'halaman' => strtoupper($artikel[0]->judul_artikel),
				'kategori' => $this->kategori,
				'arsip' => $this->arsip,
				'id' => $artikel[0]->id_artikel,
				'url' => $artikel[0]->url,
				'judul' => $artikel[0]->judul_artikel,
				'judul_kategori' => $artikel[0]->judul_kategori,
				'id_kategori' => $artikel[0]->id_kategori,
				'tanggal' => $tgl,
				'isi' => $artikel[0]->isi_artikel,
				'gambar' => $this->front_model->get_where('tb_gambar_artikel', 'id_artikel', $artikel[0]->id_artikel),
				'komentar' => $komentar,
				'jumlah_komentar' => count($komentar)
			);
			$this->output->set_meta('description', substr($artikel[0]->isi_artikel,0,200));
		} else {
			$data = array(
				'halaman' => '404',
				'kategori' => $this->kategori,
				'arsip' => $this->arsip
			);
		}
		$this->load->view('webfront/artikel-detail', $data);
	}

	public function komen(){
		$id = $this->input->post('id');
		$url = $this->input->post('url');
		$nama = $this->input->post('nama', true);
		$email = $this->input->post('email', true);
		$isi = $this->input->post('isi', true);
		$data =  array(
			'id_artikel' => $id,
			'nama' => $nama,
			'email' => $email,
			'isi_komentar' => $isi,
			'tgl' => date('Y-m-d H:i:s')
		);
		$komentar = $this->front_model->insert('tb_komentar', $data);
		if ($komentar==true) {
			redirect('home/artikel_detail/'.$url);
		} else {
			echo "Gagal menyimpan komentar. <a href='".base_url()."home/artikel_detail/".$url."'>Kembali ke halaman artikel</a>";
		}
	}

	public function arsip(){
		$this->output->set_meta('description', 'Arsip artikel');
		$bulan = $this->uri->segment(3);
		$tahun = $this->uri->segment(4);

		//pagination settings
        $config['base_url'] = base_url().'home/arsip/'.$bulan.'/'.$tahun.'/';
        $config['total_rows'] = $this->front_model->count_where_2('tb_artikel', 'MONTH(tgl)=', $bulan, 'YEAR(tgl)=', $tahun);
        $config['per_page'] = "4";
        $config["uri_segment"] = 5;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="#" class="selected">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
				$data['hal'] = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

				$artikel = $this->front_model->get_data_arsip($bulan, $tahun, $config["per_page"], $data['hal']);

		$data = array(
			'halaman' => 'Arsip',
			'kategori' => $this->kategori,
			'arsip' => $this->arsip,
			'pagination' => $this->pagination->create_links(),
			'artikel' => $artikel,
			'jumlah' => count($artikel)
		);
		$this->load->view('webfront/artikel', $data);
	}

}
