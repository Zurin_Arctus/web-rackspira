<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lancelot extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('front_model');
		$this->load->model('login_model');
		$this->_init();
	}

	private function _init()
	{
    $this->output->set_template('admin-login');
		// <!-- Bootstrap core CSS     -->
		$this->load->css('assets/themes/admin/assets/css/bootstrap.min.css');
		// <!--  Material Dashboard CSS  -->
		$this->load->css('assets/themes/admin/assets/css/material-dashboard.css');
		// <!--  CSS for Demo Purpose, don't include it in your project  -->
		$this->load->css('assets/themes/admin/assets/css/demo.css');
		// <!--  Fonts and icons  -->
		$this->load->css('assets/themes/admin/assets/css/font-awesome.min.css');
		$this->load->css('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons');
		// <!--   Core JS Files   -->
		$this->load->js('assets/themes/admin/assets/js/jquery-3.1.1.min.js');
		$this->load->js('assets/themes/admin/assets/js/jquery-ui.min.js');
		$this->load->js('assets/themes/admin/assets/js/bootstrap.min.js');
		$this->load->js('assets/themes/admin/assets/js/material.min.js');
		$this->load->js('assets/themes/admin/assets/js/perfect-scrollbar.jquery.min.js');
		$this->load->js('assets/themes/admin/assets/js/jquery.validate.min.js');
		$this->load->js('assets/themes/admin/assets/js/sweetalert2.js');
		$this->load->js('assets/themes/admin/assets/js/material-dashboard.js');
		$this->load->js('assets/themes/admin/assets/js/demo.js');
	}

	public function index()
	{
		$this->load->view('admin/login');
	}

	public function login_proses()
	{
		$login = $this->login_model->login_admin();
		if ($login==true) {
			redirect(base_url().'arthur');
		} else {
			$this->session->set_flashdata('login_error', 'Username atau password salah');
			redirect(base_url().'lancelot');
		}
	}

}
